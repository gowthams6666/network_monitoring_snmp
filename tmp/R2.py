from twisted.internet.defer import DeferredList
#from twisted.internet.task import react
from twisted.internet import reactor
from pysnmp.hlapi.twisted import *
from threading import Thread
import csv
import sqlite3
import time
sync_req = 0
import os

################
# pysnmp bug need to fix in :C:\Python27\lib\site-packages\twisted\python\failure.py
# need to command : #raise TypeError("Strings are not supported by Failure")  to prevend wrong diest exceptions
##############

class dbcon():
    def __init__(self):
        pass;
    def dbupdate(self,d):
        try:
            #d = {"uid":2,"name":"gowtham","ip":"","oid":"","data":"","note":"","utime":""}
            self.conn = sqlite3.connect('live_snmp.db')
            #conn.execute("UPDATE COMPANY set NAME = 'GPPP', ID = '2' where ID=1'")
            #conn.commit()
            #if conn.total_changes < 1:
            self.conn.execute("INSERT OR REPLACE INTO COMPANY (ID,NAME,IP,OID,DATA,NOTE,UTIME) VALUES (?,?,?,?,?,?,?)",
                         (d['uid'],d['name'],d['ip'],d['oid'],d['data'],d['note'],d['utime']))
            self.conn.commit()
            #print self.conn.total_changes
        except Exception as e:
            print "#####>4",e


def read_csv(usr_input="all"):
    try:
        csv_dic = {}
        csv_read = csv.DictReader(open("test.csv","r"))
        if usr_input == "all":
            return csv_read
        for csv_data in csv_read:
            if str(csv_data).find(usr_input) != -1:
                print "Search Found:",csv_data
                return csv_data
        return None
    except Excpetion as e:
        print "read_csv Error :"+str(e)
        return None

class snmp_all():
    def __init__(self):
        self.snmpEngine = SnmpEngine()
        #self.usrname = 'NPCI_CTS_NMSV3'
        #self.passwd = 'npcictsnms'
        #self.moid = ".1.3.6.1.4.1.9.9.387.1.1.2.1.4.0"
        self.dbdata = dbcon()
        #self.dbdata.cachdb()

    def create_engine(self):
        try:
            self.eng_p1 = SnmpEngine()
            varBinds = ObjectType(ObjectIdentity(".1.3.6.1.2.1.1.5.0"))
            auth = UsmUserData("NPCI_CTS_NMSV3","npcictsnms",privKey=None,privProtocol=eval("cmdgen.usmNoPrivProtocol"),authProtocol=eval("cmdgen.usmHMACMD5AuthProtocol"))
            getCmd(self.eng_p1,auth,UdpTransportTarget(("127.0.0.1","161")),ContextData(),varBinds)

            self.eng_p2 = SnmpEngine()
            varBinds = ObjectType(ObjectIdentity(".1.3.6.1.2.1.1.5.0"))
            auth = UsmUserData("NPCI_CTS_NMSV3","npcictsnms",privKey=None,privProtocol=eval("cmdgen.usmNoPrivProtocol"),authProtocol=eval("cmdgen.usmHMACMD5AuthProtocol"))
            getCmd(self.eng_p2,auth,UdpTransportTarget(("127.0.0.1","161")),ContextData(),varBinds)
        except Exception as e:
            print "#####>3",e
        
    def success(self,(errorStatus, errorIndex, varBindTable), snmpEngine, data):
        global sync_req
        val = "none"
        sync_req = sync_req + 1
        hostname = data['ip']
        try:
            if errorStatus:
                print str(hostname)+">"+"Error Stat"
                #print errorStatus
                val = "none"
            else:
                for name, val in varBindTable:
                    print str(hostname)+">"+str(val)
                    val = str(val)
        except Exception as e:
            print str(hostname[0])+">"+"Error"
            #print e
            
        d = {"uid":int(data['id']),"name":data['name'],"ip":hostname,"oid":data['oid'],"data":val,"note":"sucess","utime":str(time.time())}
        self.dbdata.dbupdate(d)

    def failure(self,errorIndication,data):
        global sync_req
        sync_req = sync_req + 1
        hostname = data['ip']
        val = "none"
        note = ""
        try:
            if str(errorIndication).find("wrongDigest") != -1:
              print str(hostname)+">"+"Wrong Digest"
              note = "Wrong Digest"
            else:
                print str(hostname)+">"+"Timeout"
                note = "Timeout"
            #return "Timeout"
        except Exception as e:
            print "#####>2",e
        
        d = {"uid":int(data['id']),"name":data['name'],"ip":hostname,"oid":data['oid'],"data":val,"note":note,"utime":str(time.time())}
        self.dbdata.dbupdate(d)

    def get_nxt(self,snmpEngine,data):
        try:
            global sync_req
            #auth = CommunityData("NPCI_CTS_CSMR")
            hostname = (data['ip'],"161")
            usrname = data["susername"]
            passwd = data["spassword"]
            moid = data['oid']
            rty = int(data["retry"])
            if rty < 1:
            	rty = 1;
            varBinds = ObjectType(ObjectIdentity(moid))
            #snmpEngine.cache.update({'__mibViewController':mv})
            #print snmpEngine.cache.get('tran')
            #snmpEngine.delUserContext('auth')
            #snmpEngine.delUserContext('addr')
            #snmpEngine.delUserContext('__automaticTransportDispatcher')
            #snmpEngine.delUserContext('__getTargetAddr')
            #snmpEngine.delUserContext('__getTargetParams')
            #snmpEngine.delUserContext('__CommandGeneratorLcdConfigurator')
            auth = UsmUserData(usrname,passwd,privKey=None,privProtocol=eval("cmdgen.usmNoPrivProtocol"),authProtocol=eval("cmdgen.usmHMACMD5AuthProtocol"))
            d = getCmd(snmpEngine,
                        auth,
                        UdpTransportTarget(hostname,timeout=int(data["timeout"]),retries=int(rty)),
                        ContextData(),
                        varBinds)
            d.addCallback(self.success, snmpEngine, data).addErrback(self.failure,data)
            return d
        except Exception as e:
            sync_req = sync_req + 1
            print ">>",e


    def start_eng(self,data):
        #while True:
        #    raw_input(">>")
        #return DeferredList([self.get_nxt(self.snmpEngine,d)for d in data])
        try:
            for d in data:
                self.snmpEngine = SnmpEngine()
                self.get_nxt(self.snmpEngine,d)
        except Exception as e:
            print "#####>1",e
        

class start_mon():
    def __init__(self):
        global sync_req
        self.sa = snmp_all()
        self.data = list(read_csv())
        #self.sa.create_engine()
        sync_req = len(self.data)
        print ">",sync_req
        self.stop_re = 0
    def loop_snmp(self,s):
        global sync_req
        #print ">",sync_req
        if sync_req >= len(self.data):
            if self.stop_re == 1:
                reactor.stop()
            print "NEW SESSION STARTED"
            sync_req = 0
            self.sa.start_eng(self.data)
            print "\n========"
            self.stop_re = 1
        reactor.callLater(0.5, self.loop_snmp, "")
        
    def mon_me(self):
        try:
            reactor.callLater(2, self.loop_snmp, "")
            reactor.run()
        except Exception as e:
            print "#####>6",e


#dc = dbcon()
#dc.cachdb()
#dc.dbupdate()

sm = start_mon()
sm.mon_me()

